# Workflow for the numerical modelling of mountain glaciers ice flow

This directory contains every step of the process to model any ensemble of mountain glaciers, given enough computing power. The workflow presented here runs on a local computed. To run the most demanding steps of the process, some adjustments might be needed.

On your computer, you need Elmer/Ice to be installed, as well as the Open Global Glacier Model (OGGM).
Is also needed the following python libraries:
-numpy
-xarray
-rasterio
-pandas
-geopandas
-os

# Workflow

- **1_glacier_selection_and_merging**: The first step is to merge the RGI entities whose outlines are connected into *groups* of glaciers. Once this process is done, the groups that we want to model can be selected.

- **2-DEMs_merging**: The thickness database of Farinotti et al. (2019) gives different bedrocks for every RGI entities. This steps merges the bedrocks of glacier groups defined in step *1* into a single file.

- **3_surface_velocities_fitting**: This step is used the different DEMs for a given glacier group to compute its velocity field using Elmer/Ice. It is possible to specify different parameters for the deformation and sliding of the ice. Once the velocity field for every combination of parameters/bedrock is done, the surface velocity is compared to the sattelite observations of Millan et al.() of the surface velocity. The best couples are then retained for the step *4*

- **4_transient_simulations**: Using OGGM Surface Mass Balance parameterization of the glaicer groups selected, a spinup run and different forcing scenarios are created. The evolution of all the glacier groups selected is computed, given their different parameterization determined in step *3*.
