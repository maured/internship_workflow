

import os
import geopandas as gpd


grouplist = '../1_glacier_selection_and_merging/glacier_group_list.txt'
shp_file = '../1_glacier_selection_and_merging/rgi_grouped/rgi.shp'

shp = gpd.read_file(shp_file)

groups = open(grouplist).read().splitlines()

os.system('rm -r DEMs/bedrock')
os.system('rm -r DEMs/surface')

os.mkdir('DEMs/bedrock')
os.mkdir('DEMs/surface')

for group in groups:
    sub_glaciers = shp[shp.glacier_gr == int(group)]
    os.mkdir('DEMs/bedrock/'+group)
    
    for idx, glacier in sub_glaciers.iterrows():
        rgi_id = glacier.RGIId
        
        for model in os.listdir('../../data/thickDEMs'):
            try:
                os.mkdir('DEMs/bedrock/'+group+'/'+model)
            except:
                print()
            os.system('cp ../../data/thickDEMs/'+model+'/'+rgi_id[:8]+'/thickness_'+rgi_id+'.tif DEMs/bedrock/'+group+'/'+model+'/'+rgi_id+'.tif')

os.system('sh 2.2-merge_dems.sh')
