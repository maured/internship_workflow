cd DEMs/bedrock

	for group in *;do
		cd $group
		for model in *;do
			cd $model
			gdal_merge.py -o merged.tif -q -n 0 `ls *.tif`
			gdal_translate -of netcdf merged.tif thick.nc
			cd ..
			done
		cd ..
		done

