function ncdx { ncap2 -O -C -v -s "foo=(${1}.max()-${1}.min())/(${1}.size()-1);print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; } 


echo $dx $dy


export HOME_DIR=$(pwd)



file='src/~A-c-fit/glaciergrouplist.txt' #
cd $1
cd $2
	for model in *; do
		cp -RT ../../../../params/sif_contents $model
		cd $model
		sed -e "s/<A>/$1/g;s/<C>/$2/g;" ../../../../../params/sif_contents/Parameters.lua > Parameters.lua

		#ElmerSolver INIT.sif
		mpirun -np 8 ElmerSolver INIT.sif
		dx=$(ncdx x INPUT/surface.nc)
		echo $dx
		sed -e "s/<cell_size>/$dx/g;" steady.sif > tmp.sif
		

		rm steady.sif
		mv tmp.sif steady.sif

		#ElmerSolver steady.sif
		mpirun -np 8 ElmerSolver steady.sif
	
		cd ..
	done
cd $HOME_DIR
	

