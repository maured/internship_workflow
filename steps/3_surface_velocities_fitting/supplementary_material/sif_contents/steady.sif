!---LUA BEGIN
! assert(loadfile('Parameters.lua'))()
! assert(loadfile('LuaFunctions.lua'))()
!---LUA END

! Local Parameters
#MESH="rectangle"
#name="1"

#Startyear = 0.0

#dt=0.5
#Ndt=1

#OutInter=1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Header
  Mesh DB "INPUT/#MESH#" "."
  Results Directory "OUTPUT"
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Simulation
  Coordinate System  =  Cartesian 3D 

  Simulation Type = Transient
  Timestepping Method = "bdf"
  BDF Order = 2

  Timestep Intervals(1) = #Ndt
  Output Intervals(1) = #OutInter
  Timestep Sizes(1) = #dt 

  Steady State Max Iterations = 1
  Steady State Min Iterations = 1

  !! Internal Extrusion
  include EXTRUDE.IN 

  Output File = "RUN_#name#_.result"
  Post File = "RUN_#name#_.vtu"
  vtu:VTU Time Collection = Logical True

  Restart File = "RUN_0_.result "
  Restart Position = 0
  Restart Time = Real #Startyear
  Restart Before Initial Conditions = Logical True 

  max output level = 4
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Body 1
  Equation = 1
  Body Force = 1
  Material = 1
  Initial Condition = 1
End
Body 2
  Name= "surface"
  Equation = 2
  Material = 1
  Body Force = 2
  Initial Condition = 2
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Initial Condition 1
  Top IcyMask = Real 1.0
End

Initial Condition 2
  Zs= Equals surface
  Ref Zs = Equals surface       
  
  IcyMask = Real 1.0
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Body Force 1
  Flow BodyForce 1 = Real 0.0    
  Flow BodyForce 2 = Real 0.0            
  Flow BodyForce 3 = Real #gravity 

  !Velocity 1 = Real 0.0
  !Velocity 2 = Real 0.0
  !Velocity 3 = Real 0.0
  !Velocity 1 Condition = Variable "Top IcyMask"        
  !  Real LUA "IfThenElse(tx[0]< -0.5, 1.0, -1.0)" 
  !Velocity 2 Condition = Variable "Top IcyMask"        
  !  Real LUA "IfThenElse(tx[0]< -0.5, 1.0, -1.0)" 
  !Velocity 3 Condition = Variable "Top IcyMask"        
  !  Real LUA "IfThenElse(tx[0]< -0.5, 1.0, -1.0)" 

! This should be in Body Force 2 but not working 
! for solver executed on a boundary

  Zs = Variable bed
    Real LUA "tx[0]+ MinH"
  Zs Condition = Variable IcyMask
    Real LUA "IfThenElse(tx[0]< -0.5, 1.0, -1.0)" 

 Flow Solution Passive = Variable Thickness
  Real LUA "IfThenElse(tx[0]< 5, 1.0, -1.0)" 
End

Body Force 2
  Zs Accumulation Flux 1 = real 0.0
  Zs Accumulation Flux 2 = real 0.0
  Zs Accumulation Flux 3 = Equals "Mass Balance"
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Material 1
! For the ice flow  
  Density = Real #rhoi   

  Viscosity Model = String "Glen"
  Viscosity = 1.0 ! Dummy but avoid warning output

  Glen Exponent = Real #n
  
  Critical Shear Rate = Real 1.0e-10
  Set Arrhenius Factor = Logical True
  Arrhenius Factor = Real #A

  Min Zs = Variable bed
    Real LUA "tx[0]+ MinH"
End
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Map mesh to bottom and top surfaces
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 1
  Equation = "MapCoordinate"
  Procedure = "StructuredMeshMapper" "StructuredMeshMapper"

! Extrusion direction
  Active Coordinate = Integer 3

  Displacement Mode = Logical False

! Check for critical thickness
  Correct Surface = Logical True
  Minimum Height = Real #MinH

! Top and bottom surfaces defined from variables
  Top Surface Variable Name = String "Zs"
  Bottom Surface Variable Name = String "bed"
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Compute Thickness and Depth
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 2
  Equation = "HeightDepth 1"
  Procedure = "StructuredProjectToPlane" "StructuredProjectToPlane"

  Active Coordinate = Integer 3

  Project to everywhere = Logical True

  Operator 1 = Thickness
  Operator 2 = Depth
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Icy Mask
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 3
  ! to be executed on top surface (need Thickness)
  Equation = "IcyMask"
  Procedure = "../../../../../params/IcyMaskSolver" "IcyMaskSolver"
  Variable = "IcyMask"
  Variable DOFs = 1

 ! no matrix resolution
  Optimize Bandwidth = Logical False

  Toler = Real 1.0e-1
  Ice Free Thickness = Real #MinH
  Remove Isolated Points = Logical True
  Remove Isolated Edges = Logical True
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Project Ice Mask in bulk
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 4
  Equation = "HeightDepth 2"
  Procedure = "StructuredProjectToPlane" "StructuredProjectToPlane"
  Active Coordinate = Integer 3
  Variable 1 = IcyMask
  Operator 1 = top

  Project to everywhere = Logical True
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Stokes 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 5
  !Exec Solver = String "Never"
  Equation = "Stokes-Vec"
  Procedure = "IncompressibleNSVec" "IncompressibleNSSolver"
  Stokes Flow = logical true

  Div-Curl Discretization = Logical False
  Stabilization Method = String Stabilized

  !linear settings:
  !------------------------------
  !include block_4.sif

 ! Linear System Solver = Direct
 ! Linear System Direct Method = umfpack
  Linear System Solver = Iterative
  Linear System Iterative Method = BiCGStab
  Linear System Max Iterations  = 2000
  Linear System Preconditioning = ILU2
  Linear System Convergence Tolerance = 1.0e-08

  !Non-linear iteration settings:
  !------------------------------ 
  Nonlinear System Max Iterations = 50
  Nonlinear System Convergence Tolerance  = 1.0e-5
  Nonlinear System Newton After Iterations = 50
  Nonlinear System Newton After Tolerance = 1.0e-5
  Nonlinear System Reset Newton = Logical True

  ! Convergence on timelevel (not required here)
  !---------------------------------------------
  Steady State Convergence Tolerance = Real 1.0e-3

  Relative Integration Order = -1
  !Number of Integration Points = Integer 44 ! 21, 28, 44, 64, ...

  ! 1st iteration viscosity is constant
  Constant-Viscosity Start = Logical False

End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Mass balance forcing
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 6
  Equation = SMB 
  Procedure = "../../../../../params/SyntSMB" "SyntSMB"
  Variable = "Mass Balance"
  Variable DOFs = 1

 ! no matrix resolution
  Optimize Bandwidth = Logical False

  SMB Glacier Head = Real 2.7
  SMB Glacier Head Elevation = Real 1600.0
  SMB Exponent = Real 1.0
  SMB ELA = Real 1200.0
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Free surface evolution
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Solver 7
  Exec Solver = String "Never"
  Equation =  String "Free Surface Evolution"
  Procedure = "FreeSurfaceSolver" "FreeSurfaceSolver"

  Variable = "Zs"
  Variable DOFs = 1

 ! calculate dz/dt (better than from mesh velocity in case os steady-state iterations)
  Calculate Velocity = Logical True

 ! Apply internal limiters
  Apply Dirichlet = Logical true

 ! Steb method 
  Stabilization Method = Stabilized

 ! linear settings
  Linear System Solver = Iterative
  Linear System Iterative Method = GCR
  Linear System Max Iterations  = 1000
  Linear System Preconditioning = ILU0
  Linear System Convergence Tolerance = 1.0e-10

 ! non-linear settings
  Nonlinear System Max Iterations = 100 ! variational inequality needs more than one round
  Nonlinear System Min Iterations = 2
  Nonlinear System Convergence Tolerance = 1.0e-8

  Steady State Convergence Tolerance = 1.0e-4


  Exported Variable 1 = -nooutput "Zs Residual"
  Exported Variable 2 =  "Ref Zs"
End

Solver 8
  Exec Solver = "After All"
  !Equation = SaveGrid
  Procedure = "SaveGridDataNetCDF"  "SaveGridData"

  Grid dx = Real <cell_size>
  Grid Origin At Corner = Logical True

  Vector Field 1 = Velocity
  Mask Name = String "MyMask"
  !NetCDF Format = Logical True

  Filename Prefix = String "surf_data"
  Table Format = Logical True

  Check for Duplicates = Logical False


  !NetCDF Format = Logical True
  !Filename Prefix = File "NetCDFTest"
  !Grid dx = Real 25
  !Check for Duplicates = Logical True
  !Grid Origin At Corner = Logical True
  !Mask Name = String MyMask

  !Scalar Field 1 = "pressure"
  Vector Field 1 = "velocity"
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Equation 1
  Active Solvers(4) = 1 2 4 5
End

Equation 2
  Active Solvers(4) = 3 6 7 8
  Flow Solution Name = String "Flow Solution"
  Convection = Computed
End

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Side
Boundary Condition 1
  Target Boundaries = 1
  Name = "side"

 ! pressure, gravity is negative
  External Pressure = Variable Depth
   REAL LUA "tx[0]*rhoi*gravity"

End

! Bedrock 
Boundary Condition 2

  Name = "bed"
  
  Normal-Tangential Velocity = Logical True
  Flow Force BC = Logical True

  Velocity 1 = Real 0.0
  
  Slip Coefficient 2 =  Variable Coordinate 1
    Real Procedure "ElmerIceUSF" "Sliding_Weertman"
  Slip Coefficient 3 =  Variable Coordinate 1
    Real Procedure "ElmerIceUSF" "Sliding_Weertman"
    
  Weertman Friction Coefficient = Real #C    
  Weertman Exponent = Real $1.0/3.0
  Weertman Linear Velocity = Real 0.01
  
End

! Upper Surface
Boundary Condition 3
  Name = "upper surface"
  Body Id = 2
  MyMask = Logical True
End

