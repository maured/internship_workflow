#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 14:23:13 2021

@author: maured
"""


import numpy as np
import matplotlib.pyplot as plt
import netCDF4 as nc
import xarray as xr

data = np.loadtxt( 'surf_data.dat' )
#data = np.loadtxt( '/home/maured/stage-m2-damien/ITMIX_v2/dirs/mesh_test/500/surf_data.dat' )
x = data[:,3]
y = data[:,4]
vx = data[:,6]
vy = data[:,7]
V = np.sqrt(vx*vx+vy*vy)

#the cellsize has to be read from ncfile
surf_nc = xr.open_dataset('INPUT/surface.nc')


cellsize = abs(surf_nc.x[1]-surf_nc.x[0]).item()

lx=(max(x)-min(x))
ly=(max(y)-min(y))
nx = abs(int((max(x)-min(x))/cellsize))
ny = abs(int((max(y)-min(y))/cellsize))

x0 = min(x)
y0 = min(y)

nodatavalue = -9999
grid = np.ones([nx,ny])*nodatavalue
grid_vx = np.ones([nx,ny])*nodatavalue
grid_vy = np.ones([nx,ny])*nodatavalue

for i in range(len(x)):
    j=int((x[i]-x0)/cellsize)-1
    k=int((y[i]-y0)/cellsize)-1
    if V[i]>10e-25:
        grid[j,k]=V[i]
        grid_vx[j,k]=vx[i]
        grid_vy[j,k]=vy[i]

ds = nc.Dataset('surf_speed.nc', 'w', format='NETCDF4')
lon = ds.createDimension('lon', nx)
lat = ds.createDimension('lat', ny)

lons = ds.createVariable('lon', 'f4', ('lon',))
lats = ds.createVariable('lat', 'f4', ('lat',))

lons[:] = np.arange(min(x)+cellsize/2, max(x)+cellsize/2, cellsize)
lats[:] = np.arange(min(y)+cellsize/2, max(y)+cellsize/2, cellsize)


speed = ds.createVariable('magnitude', 'f4', ('lat', 'lon',))
speed[:,:]=np.transpose(grid)
v_x = ds.createVariable('vx', 'f4', ('lat', 'lon',))
v_x[:,:]=np.transpose(grid_vx)

v_y = ds.createVariable('vy', 'f4', ('lat', 'lon',))
v_y[:,:]=np.transpose(grid_vy)

ds.close()
#vz = data[:,8]



