HOME_DIR=$(pwd)


A_list=$HOME_DIR/supplementary_material/A_list.txt
C_list=$HOME_DIR/supplementary_material/C_list.txt
glacier_list=$HOME_DIR/../1_glacier_selection_and_merging/glacier_group_list.txt

#we start by making the output surface_speed directories
while read gl ;do
	mkdir outputs/$gl
done < $glacier_list


#Now we loop over desired A values for all glgroups and models
while read group;do
cd dirs/$group
while read A ;do
	while read C; do
	echo "Starting simulations for A =" $A "and C=" $C
	bash ../../supplementary_material/solver_steady.sh $A $C

	done < $C_list
done < $A_list
cd ../..
done < $glacier_list
