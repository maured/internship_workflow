export HOME_DIR=$(pwd)

A_file=$HOME_DIR/supplementary_material/A_list.txt
C_file=$HOME_DIR/supplementary_material/C_list.txt
cd dirs
for glacier in *;do
	mkdir $HOME_DIR/outputs/$glacier
	cd $glacier
	while read A; do
		mkdir $HOME_DIR/outputs/$glacier/$A
		cd $A
		while read C; do
			cd $C
			mkdir $HOME_DIR/outputs/$glacier/$A/$C
			for model in *;do
			echo "$model"
			cd $model

			#cat surf_datapar*.dat > surf_data.dat
			python $HOME_DIR/supplementary_material/dat_to_nc.py
			cp surf_speed.nc $HOME_DIR/outputs/$glacier/$A/$C/${model}.nc

			cd ..
		done
		cd ..
	done < $C_file
	cd ..
	done < $A_file
cd ..
done
